module Refinery
  module Clients
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::Clients

      engine_name :refinery_clients

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "testimonials"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.clients_admin_testimonials_path }
          plugin.pathname = root
          plugin.menu_match = %r{refinery/clients/testimonials(/.*)?$}
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::Testimonials)
      end
    end
  end
end
