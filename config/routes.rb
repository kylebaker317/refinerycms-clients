Refinery::Core::Engine.routes.draw do

  # Admin routes
  namespace :clients, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :clients, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end
  # Frontend routes
  # namespace :clients do
  #   resources :testimonials, :only => [:index, :show]
  # end

  scope :testimonials, module: 'clients', as: 'clients' do
    resources :testimonials, path: '', :only => [:index, :show]
  end
  
  # Admin routes
  namespace :clients, :path => '' do
    namespace :admin, :path => "#{Refinery::Core.backend_route}/clients" do
      resources :testimonials, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end
  get "/admin/clients/:id(.:format)" => "clients/admin/clients#show",
      :as => :view_clients_admin_client


end
