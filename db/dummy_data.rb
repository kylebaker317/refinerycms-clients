Refinery::I18n.frontend_locales.each do |lang|
  I18n.locale = lang

  def add_fake_clients(number_of_clients, percentage)
    number_to_create = (number_of_clients - Refinery::Clients::Client.count)
    number_to_create.times do
      if percentage < rand(100) # Create Individual Client
        Refinery::Clients::Client.create first_name: Faker::Name.first_name,
                                         last_name: Faker::Name.last_name
      else
        Refinery::Clients::Client.create first_name: Faker::Name.first_name,
                                         last_name: Faker::Name.last_name,
                                         company: Faker::Company.name
      end
    end
  end


  add_fake_clients(100, 60)

  puts 'Refinery::Clients dummy_data.rb ran successfully'
end