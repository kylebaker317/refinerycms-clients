Refinery::I18n.frontend_locales.each do |lang|
  I18n.locale = lang

  def add_fake_testimonials(number_of_featured)
    counter = 0
    Refinery::Clients::Client.find_each do |client|
      counter +=1
      if client.testimonials.empty?
        new_testimonial = client.testimonials.new description: Faker::Lorem.paragraph(5)
        if counter <= number_of_featured
          new_testimonial.update_attributes(featured: true)
        end
        new_testimonial.save
      end
    end
  end

  def create_fake_testimonials(per_project) # should be more than number of clients
    project_ids = Refinery::Projects::Project.pluck(:id)
    no_of_testimonials = (project_ids.count * per_project).round
    testimonials_to_add = no_of_testimonials - Refinery::Clients::Testimonial.all.count
    testimonials_to_add.times do
      Refinery::Clients::Testimonial.create refinery_projects_id: project_ids.sample,
                                            description: Faker::Lorem.paragraph(5, false, 3)

    end
    if testimonials_to_add < 0
      puts '0 testimonials added.'
    else
      puts "#{testimonials_to_add} testimonial(s) added."
    end

  end


  create_fake_testimonials(0.5)
  puts 'Refinery::Clients testimonial_dummy_data.rb ran successfully'
end