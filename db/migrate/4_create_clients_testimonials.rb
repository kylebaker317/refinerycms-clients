class CreateClientsTestimonials < ActiveRecord::Migration

  def up
    create_table :refinery_clients_testimonials do |t|
      t.string :client
      t.integer :photo_id
      t.text :description
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-clients"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/clients/testimonials"})
    end

    drop_table :refinery_clients_testimonials

  end

end
