class AddAltClientToRefineryClientTestimonials < ActiveRecord::Migration
  def change
    add_column :refinery_clients_testimonials, :alt_client, :string
  end
end
