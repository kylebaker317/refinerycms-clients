class AddFeaturedTestimonialToRefineryClientsTestimonials < ActiveRecord::Migration
  def change
    add_column :refinery_clients_testimonials, :featured, :boolean, default: false
  end
end