class AddRefineryProjectsToRefineryClients < ActiveRecord::Migration
  def change
    change_table :refinery_projects do |t|
      t.belongs_to :refinery_client, index:true
    end
  end
end