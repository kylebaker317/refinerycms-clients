class CreateClientsClients < ActiveRecord::Migration

  def up
    create_table :refinery_clients do |t|
      t.string :first_name
      t.string :last_name
      t.date :from_date
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-clients"})
    end

    drop_table :refinery_clients

  end

end
