class AddRefineryClientsToRefineryTestimonial < ActiveRecord::Migration
  def change
    add_reference :refinery_clients_testimonials, :refinery_clients, index: true
    add_foreign_key :refinery_clients_testimonials, :refinery_clients, column: :refinery_clients_id
  end
end