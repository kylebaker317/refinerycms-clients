class ReorderColumnsInRefineryClientsTestimonials < ActiveRecord::Migration
  change_table :refinery_clients_testimonials do |t|
    t.change :alt_client, :string, after: :client
  end
end
