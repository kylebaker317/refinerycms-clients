class AddCompanyToRefineryClients < ActiveRecord::Migration
  def change
    change_table :refinery_clients do |t|
      t.string :company
    end
  end
end
