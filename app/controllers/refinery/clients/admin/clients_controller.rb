module Refinery
  module Clients
    module Admin
      class ClientsController < ::Refinery::AdminController

        crudify :'refinery/clients/client',
                :title_attribute => 'first_name'

        def new
          @client = Client.new
          @client.build_address
        end

        private

        # Only allow a trusted parameter "white list" through.
        def client_params
          params.require(:client).permit(:first_name, :last_name, :from_date,
                                         address_attributes: [:id, :line1, :line2, :city, :state, :zip])
        end
      end
    end
  end
end
