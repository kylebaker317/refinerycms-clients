module Refinery
  module Clients
    module Admin
      class TestimonialsController < ::Refinery::AdminController
        before_action :find_all_clients

        crudify :'refinery/clients/testimonial',
                title_attribute: 'client_name'
                # title_attribute: 'alt_client' || 'refinery_clients.full_name'
        # https://github.com/refinery/refinerycms/blob/128268047492ae73d10c4d5a013454893cc3bb67/core/lib/refinery/crud.rb

        protected
        def find_all_clients
          @clients = Refinery::Clients::Client.all
        end

        private

        # Only allow a trusted parameter "white list" through.
        def testimonial_params
          params.require(:testimonial).permit(:client, :alt_client, :photo_id, :description, :refinery_clients_id,
                                              :featured)
        end

      end
    end
  end
end
