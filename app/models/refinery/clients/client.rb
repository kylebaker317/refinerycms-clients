module Refinery
  module Clients
    class Client < Refinery::Core::BaseModel
      self.table_name = 'refinery_clients'
      include ActiveModel::Validations

      # Checks to see if first and last name are present unless a company name is provided.
      validates_presence_of :first_name, :last_name, unless: Proc.new { |a| a.company.present? }
      has_many :projects, :class_name => '::Refinery::Projects::Project', foreign_key: 'refinery_clients_id'
      #has_many :testimonials, foreign_key: 'refinery_clients_id'
      has_many :testimonials, through: :projects

      has_one :address, :as => :addressable
      accepts_nested_attributes_for :address

      def full_name
        if self.company.nil?
          "#{self.first_name} #{self.last_name}"
        else
          "#{self.company} (#{self.first_name} #{self.last_name})"
        end
      end

      def name_or_company
        if self.company.nil?
          "#{self.first_name} #{self.last_name}"
        else
          "#{self.company}"
        end
      end

      # To enable admin searching, add acts_as_indexed on searchable fields, for example:
      #
      acts_as_indexed :fields => [:first_name, :last_name]

    end
  end
end