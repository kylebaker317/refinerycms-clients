module Refinery
  module Clients
    class Testimonial < Refinery::Core::BaseModel

      # validates :client, :presence => true
      validates_presence_of :client, unless: Proc.new { |a| a.alt_client.present? }
      validates :description, :presence => true


      belongs_to :photo, :class_name => '::Refinery::Image'
      belongs_to :refinery_clients, :class_name => '::Refinery::Clients::Client'
      belongs_to :refinery_projects, class_name: '::Refinery::Projects::Project'
      alias_attribute :project, :refinery_projects
      alias_attribute :client, :refinery_clients

      # To enable admin searching, add acts_as_indexed on searchable fields, for example:
      #
      #   acts_as_indexed :fields => [:title]

      def client_name
        if self.refinery_clients.blank?
          self.alt_client
        else
          self.refinery_clients.full_name
        end
      end

    end
  end
end
