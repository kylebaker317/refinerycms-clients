require 'spec_helper'

module Refinery
  module Clients
    describe Testimonial do
      describe "validations", type: :model do
        subject do
          FactoryGirl.create(:testimonial,
          :client => "Refinery CMS")
        end

        it { should be_valid }
        its(:errors) { should be_empty }
        its(:client) { should == "Refinery CMS" }
      end
    end
  end
end
