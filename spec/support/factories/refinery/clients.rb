
FactoryGirl.define do
  factory :client, :class => Refinery::Clients::Client do
    sequence(:first_name) { |n| "refinery#{n}" }
  end
end

