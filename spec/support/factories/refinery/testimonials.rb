
FactoryGirl.define do
  factory :testimonial, :class => Refinery::Clients::Testimonial do
    sequence(:client) { |n| "refinery#{n}" }
  end
end

